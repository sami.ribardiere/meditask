import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:nut/login/log.dart';
import 'package:provider/provider.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

        initialRoute: '/login',
        routes: {
          '/login': (context) => LoginScreen(),

        }
    );
  }
}


