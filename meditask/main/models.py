from django.db import models
import datetime
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django_resized import ResizedImageField
from django.contrib.postgres.fields import ArrayField

def user_directory_path(instance, filename):
    return 'images/{filename}'.format(filename=filename)
# Create your models here.
class User(AbstractUser):
    username = models.CharField(max_length=255)
    email = models.CharField(max_length=255,unique= True)
    password = models.CharField(max_length=255)
    profilepic =  ResizedImageField(size=[300, 300],
        null=True,upload_to=user_directory_path,blank=True, default='images/Lasagna.jpg'
    )
    
    isemailvalid = models.BooleanField(default = False)
    isnexuser = models.BooleanField(default = False)
    age = models.IntegerField()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']



class SignUpToken(models.Model) :
    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    code = models.IntegerField()