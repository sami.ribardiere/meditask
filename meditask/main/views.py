from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view


from .serializers import *
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from .models import *
from rest_framework.parsers import MultiPartParser, FormParser
from django.urls import reverse
from rest_framework import generics
from django.contrib.sites.shortcuts import get_current_site
from rest_framework_simplejwt.tokens import RefreshToken
import random
from .utils import Util


# Create your views here.
class RegisterView(APIView):
    parser_classes = (MultiPartParser, FormParser)
    def post(self,request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid() :
            serializer.save()
            user_data = serializer.data
            user=User.objects.get(email=user_data['email'])
            token=RefreshToken.for_user(user).access_token

            current_site=get_current_site(request).domain

            relativeLink = reverse('email_verify')

            randomnumber = random.randrange(100000, 999999)
            absurl='http://'+current_site+relativeLink+"?token="+str(token)
            email_body='Dear ' + user.username + ',\n' 'Please use the number below in the application to verify your account \n' + str(randomnumber)
            data={'email_body':email_body, 'to_email':user.email, 'email_subject':'Verify your email '}
            Util.send_email(data)
            
            s = SignUpTokenSerializer(data = {
                'user' : user.id,
                'code' : randomnumber
                })
            if s.is_valid() :
               s.save()
                

            
            
            return Response(
                {
                    'msg' : 'success',
                    'code' : randomnumber,
                    'id' : user.id,
                    'msg3' : s.data,
                    
                    
                }
            )
        return Response(
                {
                    'msg' : 'Username or email are already used'  
                }
            )


class EmailPageView(APIView):
    def post(self,request):
        user_id = request.data['userid']
        user_code = request.data['code']
        user = User.objects.filter(pk=user_id).first()
       
        usertoken = SignUpToken.objects.filter(user=user_id).first()
        if (int(user_code) == usertoken.code):
            user.isemailvalid = True
            user.save()
            usertoken.delete()
            return Response({
                'msg' : 'Email Verified Successfully',
            })

        return Response ({
            'msg' : 'Wrong code'
        })




# user = User.objects.get(pk=serializer.data['id'])
           # p = Physic(user=user)
           # p.save()
class VerifyEmail(generics.GenericAPIView):
    def get(self):
        pass




class LoginView(APIView):
     def post(self,request):
        email = request.data['email']
        password = request.data['password']

        user = User.objects.filter(email=email).first()
        
        s = UserSerializer(user)


        if user is None:
            return Response({
            'msg' : 'User not found !',
            'detail': 0,
            'name' : 'NA',
            'image' : 'NANA',
        })

        if not user.check_password(password):
            return Response({
            'msg' : 'Incorrect password !',
            'detail': 0,
            'name' : 'NA',
            'image' : 'NANA',
        })

        if (user.isnexuser == False):
             return Response({
            'msg' : 'go to perso page',
            'detail': user.id,
            'name' : user.username,
            'image' : s.data['image_memory'],
        })


        
        
        

        response = Response()

        
        response.data = {
                'detail': user.id,
                'name' : user.username,
                'image' : s.data['image_memory'],
                'msg' : 'success',
            }
       
        if (user.isemailvalid==True):
            return response
        return Response({
            'msg' : 'user did not validate email',
            'detail': 0,
            'name' : 'NA',
            'image' : 'NANA',
        })


        
       

        
       

        
        

        
    

        
