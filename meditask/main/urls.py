from django.urls import path
from . import views
from .views import *


urlpatterns = [
    path('register', RegisterView.as_view()),
    path('login',LoginView.as_view()),
    path('email_verify', VerifyEmail.as_view(), name="email_verify"),
    path('emailPage', EmailPageView.as_view()),

]