from rest_framework import serializers
from .models import *
from drf_extra_fields.fields import Base64ImageField
import base64


class UserSerializer(serializers.ModelSerializer):
    image_memory=serializers.SerializerMethodField("get_image_memory2")
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password','profilepic','image_memory','age']
    
    def get_image_memory2(request,user:User):
            with open("media/"+user.profilepic.name, 'rb') as loadedfile:
                return base64.b64encode(loadedfile.read())
            return  

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance



class SignUpTokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = SignUpToken
        fields = ['id', 'timestamp','user','code']